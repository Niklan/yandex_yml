**This project currently is for testing purposes.**

## TODO

Add services for:

- [x] Currency
- [x] Category
- [x] Delivery
- [x] ShopInfo
- [ ] Offers
    - [ ] Simple
    - [ ] Custom
    - [ ] Medicine
    - [ ] Book
    - [ ] Audiobook
    - [ ] MusicVideo
    - [ ] EventTicket
    - [ ] Tour